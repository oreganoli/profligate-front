# profligate-front

React UI for the [`profligate`]("https://github.com/oreganoli/profligate/") Caesar cipher library using its [WebAssembly bindings]("https://gitlab.com/oreganoli/profligate-front-rs).

## Features

- encryption and decryption with a manually specified key
- automatic decryption based on known plaintext
- automatic decryption based on English letter frequency tables

## Screenshots

![Encryption](encrypt.png)
![Automatic decryption](decrypt.png)
