import { init } from "profligate-front-rs";
init();
import React from "react";
import ReactDOM from "react-dom";
import { App } from "./components/App";
let root = document.querySelector("main");
ReactDOM.render(<App />, root);