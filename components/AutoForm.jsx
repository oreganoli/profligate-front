import React, { useState } from "react";
import { decrypt_auto_crib, decrypt_auto_english } from "profligate-front-rs";

export const AutoForm = () => {
    let [method, setMethod] = useState("english");
    let [threshold, setThreshold] = useState(0.75);
    let [crib, setCrib] = useState("");
    let [input, setInput] = useState("");
    let [output, setOutput] = useState("");
    return <div>
        <h2>Automatic decryption</h2>
        <p>Automatically decrypt a string by computing the most likely keys and sequentially trying them out until a likely plaintext candidate is found.</p>
        <form>
            <div>
                <h3>Validation method</h3>
                <input id="engRadio" type="radio" checked={method == "english"} onChange={() => setMethod("english")}></input>
                <label htmlFor="engRadio">English word list</label>
                <div>
                    <label className={method != "english" ? "grayedOut" : null} htmlFor="engThreshold">Minimum proportion of intelligible words: </label>
                    <input disabled={method != "english"} id="engThreshold" type="range" min={0.01} max={1} step={0.01} value={threshold} onChange={(e) => setThreshold(e.target.value)}></input>
                    <span>{Math.floor(threshold * 100)}%</span>
                </div>
                <input id="cribRadio" type="radio" checked={method == "crib"} onChange={() => setMethod("crib")}></input>
                <label htmlFor="cribRadio">Crib (known plaintext)</label>
                <div>
                    <label className={method != "crib" ? "grayedOut" : null} htmlFor="cribInput">Crib: </label>
                    <input disabled={method != "crib"} id="cribInput" type="text" value={crib} onChange={(e) => setCrib(e.target.value)}></input>
                </div>
                <h3><label htmlFor="autoInput">Input</label></h3>
                <textarea id="autoInput" value={input} onChange={(e) => setInput(e.target.value)}></textarea>
                <div>
                    <button type="submit" disabled={
                        !(method == "crib" ? crib.length > 0 : true && input.length > 0)
                    } onClick={() => {
                        if (method == "english") {
                            setOutput(decrypt_auto_english(input, threshold));
                        } else {
                            setOutput(decrypt_auto_crib(input, crib));
                        }
                    }}>Submit</button>
                </div>
                <h3>Output</h3>
                <pre>{output}</pre>
                <button disabled={output.length <= 0} onClick={async () => {
                    await navigator.clipboard.writeText(output)
                }}>Copy</button>
            </div>
        </form>
    </div>
}