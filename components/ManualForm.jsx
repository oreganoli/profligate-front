import React, { useState } from "react";
import { encrypt, decrypt } from "profligate-front-rs";

const ManualForm = (props) => {
    const encrypting = props.current_tab == "encrypt";
    const [input, setInput] = useState("");
    const [key, setKey] = useState(0);
    const [output, setOutput] = useState("");
    return <div>
        <h2>{encrypting ? "Manually encrypt" : "Manually decrypt"}</h2>
        <form>
            <div>
                <h3><label htmlFor="manualInput">Input</label></h3>
                <textarea id="manualInput" onChange={e => setInput(e.target.value)} value={input}></textarea>
            </div>
            <div>
                <p><label htmlFor="keyInput">{`Shift: ${key}`}</label></p>
                <input id="keyInput" type="range" min={-25} max={25} step={1} value={key} onChange={e => setKey(e.target.value)}></input>
            </div>
            <button disabled={input.length <= 0} type="submit" onClick={
                e => {
                    e.preventDefault();
                    if (encrypting) {
                        setOutput(encrypt(input, key));
                    } else {
                        setOutput(decrypt(input, key));
                    }
                }
            }>Submit</button>
        </form>
        <h3>Output</h3>
        <pre>{output}</pre>
        <button disabled={output.length <= 0} onClick={async () => {
            await navigator.clipboard.writeText(output)
        }}>Copy</button>
    </div>;
}
export default ManualForm;