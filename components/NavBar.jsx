import React from "react";
export const NavBar = (props) => {
    return <nav>
        {props.current_tab === "encrypt"
            ? <b>Encrypt</b>
            : <a href="#" onClick={() => { props.tabCallback("encrypt"); }}>Encrypt</a>
        }
        |
        {props.current_tab === "decrypt"
            ? <b>Decrypt</b>
            : <a href="#" onClick={() => { props.tabCallback("decrypt"); }}>Decrypt</a>
        }
        |
        {props.current_tab === "auto"
            ? <b>Automatic decryption</b>
            : <a href="#" onClick={() => { props.tabCallback("auto"); }}>Automatic decryption</a>}
    </nav>;
};