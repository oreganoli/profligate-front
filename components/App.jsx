import React, { useState } from "react";
import { AutoForm } from "./AutoForm.jsx";
import ManualForm from "./ManualForm.jsx";
import { NavBar } from "./NavBar.jsx";

const Tab = (props) => {
    switch (props.current_tab) {
        case "encrypt":
            return <ManualForm current_tab={props.current_tab} />;
        case "decrypt":
            return <ManualForm current_tab={props.current_tab} />;
        case "auto":
            return <AutoForm />;
        default:
            return null;
    }
}

export const App = () => {
    const [tab, setTab] = useState("encrypt");
    const onSetTab = (val) => {
        //console.log(val);
        setTab(val);
    }
    return <><NavBar current_tab={tab} tabCallback={(val) => onSetTab(val)} /><Tab current_tab={tab} /></>
}